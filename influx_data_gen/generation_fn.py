from uuid import uuid4 as uuid
from generate import Keyval
from random import randint
from time import time

def pick_random_from_file(file : str):
    '''
    Pick a random line from a file

    parameters: 
        file : str
            File to take the line from
    '''

    with open(file, "r") as f:
        lines = f.readlines()
        l = randint(0, len(lines)-1)
        return lines[l].replace("\n", "")


def random_time_range():
    '''
    Returns two timestamps, within the month of the current date
    which are separated by up to an hour.
    '''
    before = randint(0, 3600*31)
    after = randint(180, 3600)
    t = time()
    return t-before, t+after

def gen_user(fn_args):
    user_id = uuid()

    name = ""

    if randint(0, 100) < 50: # 50% chance to be female or male
        name = pick_random_from_file("data/female_first_names.txt")
    else:
        name = pick_random_from_file("data/male_first_names.txt")
    
    name += " " + pick_random_from_file("data/last_names.txt")

    return [Keyval(name="employee_name", value=name)], [Keyval(name="uuid", value=user_id)]

def gen_device(fn_args):
    
    device = pick_random_from_file("data/device_list.txt")
    name = device  + "_" + str(uuid())[:8]
    power_consumption = randint(1, 100)

    return [Keyval(name="device_id", value=name), Keyval(name="device_type", value=device)], [Keyval(name="power_consumption", value=power_consumption)]

def gen_network(fn_args):
    '''
    fn_args : dictionary of values, with: 
        - client : influx client to make the requests from
        - wrapper : influx wrapper to make the request 
    '''

    query_get_list_of_devices = """
    from(bucket: "fakedata_2")
  |> range(start: -10y)
  |> filter(fn: (r) => r["_measurement"] == "devices")
  |> filter(fn: (r) => r["_field"] == "device_id")
  |> distinct()
  |> yield(name: "distinct")
    """

    client = fn_args["client"]
    wrapper = fn_args["wrapper"]

    list_of_devices = wrapper(client, query_get_list_of_devices)

    size = randint(1, 5)
    start = randint(1, len(list_of_devices)-5)

    network = []
    
    part_of_network = list_of_devices[start:start+size]

    for i, table in enumerate(part_of_network):
        network.append(table.records[0]['_value'])

    return [Keyval(name="network_id", value="NET-"+str(uuid())[:8])],[Keyval(name="network_devices", value=network)]

def gen_log(fn_args):
    '''
    fn_args : dictionary of values, with: 
        - client : influx client to make the requests from
        - wrapper : influx wrapper to make the request 
    '''

    query = """
    from(bucket: "fakedata_2")
  |> range(start: -10y)
  |> filter(fn: (r) => r["_measurement"] == "employees" or r["_measurement"] == "networks")
  |> filter(fn: (r) => r["_field"] == "employee_name" or r["_field"] == "network_id")
  |> distinct()
  |> yield(name: "distinct")
    """

    client = fn_args["client"]
    wrapper = fn_args["wrapper"]

    query_result = wrapper(client, query)

    employee_list = []
    network_list = []

    for i, table in enumerate(query_result):
        if table.records[0]['_field'] == "employee_name":
            employee_list.append(table.records[0]['_value'])
        else:
            network_list.append(table.records[0]['_value'])

    employee = employee_list[randint(0, len(employee_list)-1)]
    network = network_list[randint(0, len(network_list)-1)]

    ts1, ts2 = random_time_range()

    fields = [
        Keyval(name="log_id", value=f"LOG-{str(uuid())[:8]}"),
    ]

    tags = [
        Keyval(name="employee_id", value=employee),
        Keyval(name="network_id", value=network),
        Keyval(name="turn_on_ts", value=ts1),
        Keyval(name="turn_off_ts", value=ts2),
    ]

    return fields, tags