from generate import generate_data
from generation_fn import gen_user, gen_device, gen_log, gen_network

import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

def query_wrapper(client, query_str):
  query_api = client.query_api()
  tables = query_api.query(query_str, org="nolundi")
  return tables

token = os.environ.get("INFLUXDB_TOKEN")
org = "nolundi"
url = "http://intensif03.ensicaen.fr:8080/"

client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

bucket = "fakedata_2"
write_api = client.write_api(write_options=SYNCHRONOUS)

fn_args = {
  "client" : client,
  "wrapper": query_wrapper
}

generators = [
  (20,"employees", gen_user, {}), 
  (50,"devices", gen_device, {}),
  (10,"networks", gen_network, fn_args),
  (200,"logs", gen_log, fn_args)
]

for gen in generators:
  for point in generate_data(gen[0], gen[1], gen[2], gen[3]):
    print("[WRITE]", point)
    write_api.write(bucket=bucket, org="nolundi", record=point)
    time.sleep(0.1)