from dataclasses import dataclass
from influxdb_client import Point

@dataclass
class Keyval:
    name : str
    value : object

def generate_data(samples, measurement, fn, fn_args=None):
    
    data_collection = []

    for _ in range(samples):
        fieldlist, taglist = fn(fn_args)
        datapoint = Point(measurement)

        for field in fieldlist:
            datapoint.field(field.name, field.value)

        for tag in taglist:
            datapoint.tag(tag.name, tag.value)

        data_collection.append(datapoint)


    return data_collection