#
# TABLE STRUCTURE FOR: Devices
#

DROP TABLE IF EXISTS `Log`;
DROP TABLE IF EXISTS `Employees`;
DROP TABLE IF EXISTS `Devices`;

CREATE TABLE `Devices` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumption` int(11) NOT NULL,
  `network_id` int(11) NOT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (3, '540ed9c1-3fda-3ce2-b06c-a0d65d31899d', 44, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (4, '7fd64fef-8620-3633-8bcf-4e09dc274164', 89, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (22, '562eb74f-6721-349b-ba87-68dedafbee4b', 17, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (34, '4b10d2c1-9a86-3358-a7b1-e4e7fb08b81f', 29, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (37, '4fe67428-a21f-3982-a11b-8d427a1a1122', 44, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (44, '8d2109fe-8723-3c80-9198-105badf24cee', 71, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (47, '079a5944-ecf8-3aa6-b50d-c9da36eadd10', 34, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (49, 'ffeda5bc-b76f-3fc5-bc30-15f216986930', 56, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (62, '6854aa91-6898-3f79-b912-163094c53982', 73, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (67, '010a9090-1703-3c1c-803c-eb1d00c6d2af', 19, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (76, 'e1c68282-e894-37e3-a4c2-d0a01f855e52', 48, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (103, '9feb178b-d4ec-3df8-ac16-b0d24bb5a97e', 49, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (118, 'ba30da36-b56b-3e34-9244-5f2f9778e285', 47, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (120, 'a8a86de4-24e2-33b7-8710-a788d77f882b', 16, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (123, '231396bf-cc90-38f3-8d80-76a74e429ce5', 91, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (159, '498f7887-8ee9-3a24-a28b-48d982d33a62', 91, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (163, '1ce6e545-c592-3e6a-8bd6-886d9635a0bb', 34, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (167, '63c189bf-8c03-3a0e-81c5-0df5f11a4c0c', 64, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (169, '7bb3fc3a-9801-3c12-9cd1-11d410d782b6', 53, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (180, 'c10de5d7-d7eb-3bb1-8c3b-b5a13bc99c22', 11, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (183, '06eb013d-6bb3-3901-bd58-0a9b4cbc9aed', 60, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (185, '65aab0a9-10e3-33cf-a3f3-d2fe87e452eb', 73, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (187, 'f4868f5e-a359-31cb-800e-62356ca9e2bb', 71, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (223, '8d41457f-f647-3de4-aeef-a885b3885af3', 15, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (227, '6129efd9-06d4-3d9c-9bf7-5a5a6b7afbff', 13, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (229, 'ee19d146-8edd-378e-a7d8-45c0b3d47d49', 58, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (240, 'e0e1eccc-32c7-3a20-8f30-63ec27dfc723', 61, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (266, '60678dc2-c53b-3e95-a79a-c450a3270ff2', 75, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (274, '274e225c-c3e3-3583-8321-0c01db461493', 33, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (278, '69b3fba3-2a81-3f92-b4d2-699fb5275d83', 35, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (286, 'caf733d4-7305-3bbb-8c84-956022590608', 63, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (289, '3b5a9578-402a-3a78-a6a7-e9faabf3a2d3', 91, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (307, 'e23cc07f-eb4d-3b32-a4b0-a98306d8bdfd', 35, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (324, 'a190842e-f78f-3170-9946-cd9b5ba2bdb4', 31, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (334, '56c09b0f-130d-3dee-a1ac-eb00c09423bf', 40, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (339, '599e381b-cfc7-3941-ab93-03ddd513c072', 91, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (345, 'a6d4e5eb-df57-3687-9e35-488ce3ac6120', 24, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (353, '13c2728d-36a4-3f01-b27d-8fa72a2f3b5d', 63, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (360, 'f45d5aa5-9126-3ceb-9959-c56cc07e5eb6', 21, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (381, '7c53b6a8-0f9f-3f6f-9734-7c770431796a', 1, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (383, '5d66d085-0568-389b-9328-d65b08812fb3', 18, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (396, 'db15c020-7583-3fdc-9669-33dcb98b965f', 9, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (401, '6ade4694-e5c2-3b3c-bacb-175d0b406a56', 79, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (408, '3ca7d2a9-0014-37fe-a386-5c0f2fff4eba', 16, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (410, 'eccfbdfa-bbda-3362-ae38-3b3ac1253939', 19, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (412, '8125a31b-555e-37ed-8d77-099e9aa44964', 4, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (415, '401c52c3-b826-306e-9a82-35fe8b6fe1f6', 94, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (429, '38d021ab-262f-3f4e-b929-078e045bcd0b', 17, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (450, '2fdc9f9a-d405-37fa-9000-75df0858668e', 20, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (463, 'ae1a9e3e-4639-36e0-8e1e-79b8e488482e', 51, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (474, 'c5dcb856-e877-3627-b9f5-382b392ff458', 54, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (479, '98964d28-74de-3c39-944c-a43862704ad6', 2, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (483, 'a2827263-a182-3701-9234-68737ebbedfc', 53, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (484, 'aec6aa57-375b-3519-b918-e1f5fc5722af', 0, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (502, '7e9c4445-268a-3e1a-8697-285632882ba0', 6, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (510, 'b583a802-c129-3f68-b12d-2c390aa23887', 88, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (540, 'f5f17e7e-e11e-3630-b85a-80707a78013f', 19, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (554, '586b61df-e942-3e58-8687-bdf2560e7c13', 76, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (560, 'f35a7017-6d0f-3cd5-a192-ad0c58d62428', 61, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (565, '906a8558-fce6-3d10-8267-18fbe6571ee0', 14, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (578, 'def526a7-bfc1-37a8-8867-584bae290404', 76, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (585, '8b35bad5-b0f3-3307-989c-983c47431bc1', 82, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (591, 'e2016a0c-97da-3147-995b-62881b42a649', 22, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (600, 'd72c2e08-b714-3d60-8099-9b0f425cca59', 100, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (632, '9e11bfe9-67cb-32ed-a002-754f18ef8ea0', 2, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (637, 'd7f6897a-2eb1-3a2b-95e1-6d460b8ca8d2', 9, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (648, 'a7592ba2-0dd2-39ae-8dd2-5a7270e1daa6', 91, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (656, 'f888ac9b-383e-35e0-9016-ded5a0ce8f4c', 96, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (671, '8dca70ff-738a-37a6-8cb7-e4743da9c057', 86, 1);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (686, 'b217c4c7-5ca0-3257-97c7-d32f3484d84d', 88, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (690, '9a552451-fb6c-3b3c-9c9c-78810addf2c0', 21, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (702, '94d23aad-e83a-3975-8bd4-701001453d82', 12, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (703, '787a7773-f8a7-33be-b93c-bc25a10820f7', 53, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (714, '02cd455e-2bf5-3ac8-83c8-db059b980350', 27, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (731, '42ac197c-d47e-39a6-b7d6-1bef40d9fcc7', 12, 2);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (744, '22d60481-a2eb-3dd7-a199-c81bea5b6c81', 66, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (745, 'cd9b089d-016a-3301-aefc-3674b0f5490d', 55, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (754, 'dd0d59b6-ccb5-38cc-bbde-521dec574ea1', 17, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (769, '96fc1bc3-0125-34b9-8b45-76b268225833', 82, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (772, '355d848d-ca4b-3af8-a85e-1c0d8611dcb9', 19, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (778, '94aab414-47b0-3f31-928f-a152e07ca579', 1, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (811, '92d10fcd-e574-3a68-bfd4-5aa8d7da7a53', 92, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (816, '7248a9f5-dcdc-398d-862b-86c35a3826d6', 81, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (826, '1afb240b-3ae1-3785-ab57-557881e204b1', 15, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (830, 'd7b652e3-237f-361d-a5af-b2bb4fb6ee55', 86, 7);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (859, 'f0b7b52b-c4bc-32f8-acd1-376d5eb7d475', 11, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (870, 'e8bbc2d9-8488-3fda-aa90-be36e6b1a680', 57, 0);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (881, 'c20d4134-7af0-3723-a2a9-ccd3b578812c', 18, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (885, 'e0883b4e-1b3e-3bc1-8c6c-ededc4899a75', 90, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (900, '1d762707-88ad-3d98-b1e5-4011cd205ae9', 82, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (909, '52a31696-591e-3a47-9a06-1a3a6a44d403', 79, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (917, '5b18dafd-a1f9-38ac-9252-25c8f261f28b', 89, 9);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (925, '91c20bfb-7972-30b3-8afd-c4075445e785', 37, 4);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (927, '9a364d41-4470-3103-ba11-c4674d084d9a', 11, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (941, '66bce19b-48a6-3785-9287-4aa929a019b3', 45, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (944, '9be4db81-196b-30b7-a4f1-c4acb300bbf7', 3, 3);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (955, '1738bc82-36f6-31a4-a487-2340e2c0d0ff', 95, 8);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (962, '93370eeb-f990-3605-88a8-b47ca0426db6', 39, 5);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (970, '1285dffe-c7be-3f24-87af-e7ec5be6370f', 39, 6);
INSERT INTO `Devices` (`device_id`, `device_name`, `consumption`, `network_id`) VALUES (999, '6ce9f3d5-2160-35b1-9034-a109316f1192', 54, 1);


#
# TABLE STRUCTURE FOR: Employees
#

DROP TABLE IF EXISTS `Employees`;

CREATE TABLE `Employees` (
  `employee_id` int(11) NOT NULL,
  `employee_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (4, 'Federico Brekke');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (22, 'Nico Schultz Sr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (26, 'Bailee Bins');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (30, 'Providenci Jast');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (48, 'Dexter Mann');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (85, 'Delbert Schuster');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (89, 'Dr. D\'angelo Bahringer');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (105, 'Rudy Schroeder Sr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (114, 'Jess Schmidt');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (123, 'Myrtis Vandervort');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (127, 'Dr. Rebekah Thiel');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (145, 'Nikki Shanahan');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (152, 'Tremaine Pollich');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (158, 'Mr. Damion Morissette Jr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (172, 'Marshall Ritchie');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (176, 'Ashlee Bode');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (182, 'Prof. Zackery Zulauf');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (195, 'Deron O\'Conner');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (197, 'Florence Kuvalis');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (217, 'Brionna Bartell');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (226, 'Estevan Nienow');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (235, 'Michael McGlynn');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (236, 'Dennis Moen II');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (240, 'Josiah Schumm');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (255, 'Thaddeus Gleichner');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (273, 'Ignacio Conroy II');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (285, 'Nedra Witting');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (293, 'Lukas Schmidt V');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (299, 'Henriette Bailey');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (301, 'Prof. Lane Pfeffer DVM');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (307, 'Bruce Anderson MD');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (317, 'Kamille VonRueden');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (327, 'Dr. Isai Pollich');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (342, 'Gertrude Marquardt');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (347, 'Mrs. Destiny Spencer');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (350, 'Prof. Audra Shanahan');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (363, 'Tara Paucek');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (373, 'Elijah Bode');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (378, 'Mrs. Hailie Gleichner III');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (398, 'Cristobal Hettinger');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (401, 'Dr. Garland Bartoletti Jr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (403, 'Rod Rutherford');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (406, 'Torey Little');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (417, 'Mr. Roel Botsford');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (421, 'Pierce Kassulke III');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (426, 'Ms. Rhea Tremblay');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (435, 'Lonzo Schaden');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (439, 'Chandler Steuber');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (442, 'Mr. Terrill Smith IV');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (455, 'Malvina Walker');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (466, 'Emilie O\'Conner');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (468, 'Miss Lily Lind');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (496, 'Ryleigh Cole');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (518, 'Ewald Haag Sr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (523, 'Chloe Collier');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (539, 'Keon Baumbach');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (576, 'Burley Heller');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (602, 'Montana Littel');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (613, 'Miss Reanna Littel DVM');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (616, 'Horacio Simonis PhD');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (632, 'Declan McClure');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (641, 'Dr. Manley Stehr');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (642, 'Junius Cormier');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (652, 'Kaden Schmidt');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (656, 'Dr. Doyle Jacobs');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (667, 'Estelle Rogahn');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (679, 'Ford Runte II');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (702, 'Sigrid Huels II');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (707, 'Ashly Hudson');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (716, 'Maybell Glover');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (719, 'Mrs. Kiara Gorczany');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (725, 'Donavon King');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (733, 'Melyssa Dibbert');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (736, 'Mr. Kennith Hackett');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (764, 'Mr. Raven Pfeffer');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (770, 'Name Buckridge');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (819, 'Elda Bogisich');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (820, 'Kayley Rolfson');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (823, 'Mr. Eino Kautzer Jr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (826, 'Pearline Purdy Jr.');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (831, 'Kory Cormier');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (834, 'Estelle Waelchi');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (838, 'Joshuah Zemlak');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (847, 'Clifford Wyman PhD');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (849, 'Lennie Frami');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (854, 'Louisa Lehner');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (862, 'Herbert Smith PhD');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (872, 'Kendra Morissette');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (877, 'Elmore Cremin');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (892, 'Prof. Charles Kozey V');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (905, 'Angie Tillman');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (910, 'Prof. Chris Parisian');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (927, 'Wilhelm Hickle PhD');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (931, 'Katrina Reilly');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (953, 'Arvilla Welch');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (965, 'Margret Hermann');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (979, 'Alanna Goyette');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (992, 'Prof. Shawna Gleichner I');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (996, 'Dr. Easter Bins');
INSERT INTO `Employees` (`employee_id`, `employee_name`) VALUES (1000, 'Louisa Spencer');


#
# TABLE STRUCTURE FOR: Log
#

DROP TABLE IF EXISTS `Log`;

CREATE TABLE `Log` (
  `log_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `start` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `end` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`log_id`),
  KEY `FK_2` (`employee_id`),
  KEY `FK_3` (`device_id`),
  CONSTRAINT `FK_2` FOREIGN KEY (`employee_id`) REFERENCES `Employees` (`employee_id`),
  CONSTRAINT `FK_2_1` FOREIGN KEY (`device_id`) REFERENCES `Devices` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (5, 733, 703, current_timestamp(), '2001-03-06 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (21, 347, 334, current_timestamp(), '2001-05-14 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (26, 862, 870, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (52, 48, 37, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (75, 466, 474, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (79, 26, 22, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (84, 455, 463, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (98, 114, 62, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (106, 123, 67, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (111, 702, 656, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (113, 293, 266, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (119, 652, 600, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (130, 350, 339, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (141, 30, 34, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (146, 217, 180, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (152, 820, 754, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (163, 854, 859, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (212, 105, 49, current_timestamp(), '2001-03-19 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (224, 468, 479, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (226, 642, 591, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (230, 417, 408, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (236, 905, 909, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (264, 285, 240, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (282, 847, 826, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (286, 613, 560, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (291, 616, 565, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (299, 826, 772, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (310, 378, 360, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (312, 910, 917, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (314, 317, 289, current_timestamp(), '2001-02-26 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (327, 363, 345, current_timestamp(), '2001-01-05 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (343, 89, 47, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (344, 240, 223, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (346, 679, 648, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (349, 953, 941, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (354, 401, 383, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (357, 197, 169, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (358, 406, 401, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (360, 342, 324, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (362, 255, 227, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (369, 539, 510, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (380, 719, 690, current_timestamp(), '2001-07-02 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (381, 373, 353, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (407, 716, 686, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (410, 764, 731, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (421, 195, 167, current_timestamp(), '2001-02-13 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (434, 307, 286, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (439, 172, 123, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (449, 602, 554, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (468, 834, 811, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (479, 22, 4, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (482, 632, 578, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (485, 435, 415, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (510, 819, 745, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (517, 996, 970, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (522, 327, 307, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (539, 656, 632, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (548, 496, 483, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (557, 426, 412, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (568, 707, 671, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (583, 176, 159, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (591, 736, 714, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (596, 85, 44, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (600, 979, 955, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (605, 770, 744, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (612, 576, 540, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (613, 158, 120, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (614, 421, 410, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (615, 877, 885, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (623, 992, 962, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (632, 927, 925, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (633, 439, 429, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (704, 145, 103, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (705, 823, 769, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (717, 872, 881, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (720, 965, 944, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (722, 127, 76, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (738, 931, 927, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (750, 226, 183, current_timestamp(), '2001-09-29 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (774, 725, 702, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (782, 1000, 999, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (793, 518, 484, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (808, 442, 450, current_timestamp(), '2001-06-15 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (815, 892, 900, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (817, 641, 585, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (825, 849, 830, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (827, 299, 274, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (834, 152, 118, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (839, 236, 187, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (843, 831, 778, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (851, 403, 396, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (879, 398, 381, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (886, 273, 229, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (906, 4, 3, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (907, 523, 502, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (919, 235, 185, current_timestamp(), '2001-06-12 00:00:00');
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (943, 838, 816, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (960, 301, 278, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (972, 667, 637, current_timestamp(), current_timestamp());
INSERT INTO `Log` (`log_id`, `employee_id`, `device_id`, `start`, `end`) VALUES (985, 182, 163, current_timestamp(), current_timestamp());


