CREATE TABLE `Devices`
(
 `device_id`   int NOT NULL ,
 `device_name` varchar(45) NOT NULL ,
 `consumption` int NOT NULL ,
 `network_id`  int NOT NULL ,

PRIMARY KEY (`device_id`)
);

CREATE TABLE `Employees`
(
 `employee_id`   int NOT NULL ,
 `employee_name` varchar(100) NOT NULL ,

PRIMARY KEY (`employee_id`)
);

CREATE TABLE `Log`
(
 `log_id`      int NOT NULL ,
 `employee_id` int NOT NULL ,
 `device_id`   int NOT NULL ,
 `start`       timestamp NOT NULL ,
 `end`         timestamp NOT NULL ,

PRIMARY KEY (`log_id`),
KEY `FK_2` (`employee_id`),
CONSTRAINT `FK_2` FOREIGN KEY `FK_2` (`employee_id`) REFERENCES `Employees` (`employee_id`),
KEY `FK_3` (`device_id`),
CONSTRAINT `FK_2_1` FOREIGN KEY `FK_3` (`device_id`) REFERENCES `Devices` (`device_id`)
);




